import java.security.SecureRandom;

public class Utils {

    public static int[] generateInterPrimes(int max) {
        int[] primes = new int[]{generate(max), generate(max)};

        while (!areNumbersInterPrime(primes[0], primes[1])) {
            primes = new int[]{generate(max), generate(max)};
        }
        return primes;
    }

    public static boolean areNumbersInterPrime(int a, int b) {
        return getGCD(a, b) == 1;
    }

    public static int getGCD(int a, int b) {
        while (b != 0) {
            int tmp = a % b;
            a = b;
            b = tmp;
        }
        return a;
    }

    public static int generate(int max) {
        int n = new SecureRandom().nextInt(max);
        if (n < 2) n = generate(max);
        return n;
    }

    public static void main(String[] args) {
        int[] interPrime = generateInterPrimes(200);
        System.out.println(interPrime[0] + " " + interPrime[1]);
    }
}