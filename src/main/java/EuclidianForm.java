import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EuclidianForm extends JFrame {

    private JPanel mainPanel;
    private JLabel descriptionLabel;
    private JTextField a_textField;
    private JTextField b_textField;
    private JTextField n_textField;
    private JButton generateButton;
    private JButton solveButton;
    private JLabel resultlabel;
    private JLabel explanationLabel;

    public EuclidianForm() {
        initComponents();
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pack();
        generateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int[] an = Utils.generateInterPrimes(50);
                a_textField.setText(String.valueOf(an[0]));
                n_textField.setText(String.valueOf(an[1]));
                b_textField.setText(String.valueOf(Utils.generate(50)));
            }
        });

        solveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String aString = a_textField.getText();
                String bString = b_textField.getText();
                String nString = n_textField.getText();
                if (aString != null && aString.length() > 0 && aString.matches("[0-9]+") &&
                        bString != null && bString.length() > 0 && bString.matches("[0-9]+") &&
                        nString != null && nString.length() > 0 && nString.matches("[0-9]+")) {
                    int a = Integer.parseInt(aString);
                    int b = Integer.parseInt(bString);
                    int n = Integer.parseInt(nString);

                    long x = ExtendedEuclid.solveLinearEquation(a, b, n);

                    if (x == 0) {
                        JOptionPane.showMessageDialog(EuclidianForm.this, "Inversed number doesn't exist. a and n should be coprime", "Warning", JOptionPane.WARNING_MESSAGE);
                        return;
                    }

                    if (x < 0) x = n + x;

                    resultlabel.setText(String.format("Result: \n%dx%d ≡ %dmod%s", a, x, b, n));

                    explanationLabel.setText(String.format("%dx%d = %d = %d + %d*%d", a, x, a * x, b, n, (a * x - b) / n));
                } else {
                    JOptionPane.showMessageDialog(EuclidianForm.this, "Enter valid numbers or click 'Generate' button", "Warning", JOptionPane.WARNING_MESSAGE);
                }
            }
        });
    }


    private void initComponents() {
        setResizable(false);
        setContentPane(mainPanel);
        setLocation(150, 150);
    }

    public static void main(String[] args) {
        new EuclidianForm().setVisible(true);
    }
}
